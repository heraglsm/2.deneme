﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee_Controller : MonoBehaviour
{
    public static Bee_Controller instance;
    Vector3 moveAxis = new Vector3(0f, 0f, 0f);
    public float speed = 0f;

    public GameObject pipe;
    void Start()
    {
        instance = this;
    }
   
    public void ForceControl()
    {
        
        if (Input.GetKey("a"))
        {
            speed = -3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(speed, 0f, 0f);
        }
        if (Input.GetKey("d"))
        {
            speed = 3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(speed, 0f, 0f);
        }
        if (Input.GetKey("w"))
        {
            speed = 3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(0f, speed, 0f);
        }
        if (Input.GetKey("s"))
        {
            speed = -3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(0f, speed, 0f);
        }

        if (Input.GetKey("w") && Input.GetKey("d"))
        {
            speed = 3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(speed, speed, 0f);
        }

        if (Input.GetKey("w") && Input.GetKey("a"))
        {
            speed = 3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(-speed, speed, 0f);
        }
        if (Input.GetKey("s") && Input.GetKey("a"))
        {
            speed = 3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(-speed, -speed, 0f);
        }
        if (Input.GetKey("s") && Input.GetKey("d"))
        {
            speed = 3.5f;
            this.gameObject.GetComponent<ConstantForce>().force = new Vector3(speed, -speed, 0f);
        }
    }

    public void PipeMovement()
    {
        pipe.GetComponent<ConstantForce>().force = new Vector3(0f, 0f, -0.2f);
    }
    void Update()
    {
        ForceControl();
        PipeMovement();


    }
}
